(print
 (+ 5 4))
(print
 (+ "Number: " 9))

(print
 (- 10 3))
(print
 (- 3 10.2))

(print
 (* 5 5))
(print
 (* 0.25 0.5))

(print
 (/ 1 2))
(print
 (/ 1.5 0.24))
