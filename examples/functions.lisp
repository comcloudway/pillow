(defun sayhi (&optional (name "Tom"))
  (+ "Hi" " " name))

(defun square ( a )
  (* a a))

(defun err (msg &optional (temp "error: "))
  (+ temp msg))

(print
 (sayhi))
(print
 (sayhi "Jake"))

(print
 (square 5))

(print
 (err "no error" 5))
