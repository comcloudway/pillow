(defvar x 5)
(print x)

(setq x 8)
(print x)

(defvar y x)
(setq x 9)
(print x)
(print y)

(defvar name
  (read-line))
(print
 (+ "Hello" ", " name))
