use crate::types::Position;

/// basic components of lisp programming langugae
/// used to transform plain text in computer parsable content
#[derive(Debug, Clone)]
pub enum LexerType {
    /// '<text> -> Symbol(<text>)
    SingleQuote,
    /// :<key> represents a key
    Colon,
    /// new context
    OpeningBracket(usize),
    /// end of current context
    ClosingBracket(usize),
    /// simplify spaces & lines
    Empty,
    /// Integer -> Number
    Integer(isize),
    /// Floating pont number
    Float(f64),
    /// Text section
    Text(String),
    /// Variable name
    Variable(String),
    /// nil->false, t->true
    Boolean(bool),
    /// Comment (;<comment> -> //<comment>)
    Comment(String),
    /// Multiline Comment (#||<comment>||# -> /*<comment>*/)
    MultilineComment(String),
    /// used for the &optional keyword
    Ampersand
}

/// nests the LexerType
/// provides information about the position in the source file
/// allows more detailed error messages
#[derive(Debug, Clone)]
pub struct LexerItem {
    pub position: Position,
    pub item: LexerType
}
impl LexerItem {
    fn from(item: LexerType, pos: Position) -> Self {
        LexerItem { position: pos, item }
    }
}

/// a collections of errors thrown by the lexer
#[derive(Debug, Clone)]
pub enum LexerError {
    /// error thrown if lexer stumbles upon an invalid character
    InvalidCharacter(char, Position),
    /// an unmatched closing bracket at a given position
    /// thrown if more brackets are closed than were opened
    UnmatchedClosingBracket(Position),
    /// thrown if level counter is above 0 once the programm finished
    UnmatchedOpeningBracket,
    /// allows to throw unknown errors
    /// by using a given string
    /// and optionally providing the position in the source cod
    /// which threw the error
    Custom(String, Option<Position>)
}

/// the type returned by the lexer
pub type LexerOutput = Vec<LexerItem>;

pub fn lex(src: String) -> Result<LexerOutput, LexerError> {
    let mut buffer: LexerOutput = Vec::new();
    let mut level: usize = 0;

    let lines: Vec<String> = src.split('\n').map(|s|s.to_string()).collect();
    for (l, line) in lines.iter().enumerate() {
        let characters:Vec<char> = line.chars().collect();
        for (c, character) in characters.iter().enumerate() {
            let pos = Position::from_xy(c,l);

            if let Some(last) = &mut buffer.last_mut() {
                // lookbefore
                // use continue to skip other steps

                match &mut last.item {
                    LexerType::Text(t) => {
                        if let Some(l) = t.chars().last() {
                            if l != '\\' && *character == '"'{
                                // end string
                                buffer.push(LexerItem::from(LexerType::Empty, pos));
                                continue;
                            }
                        }

                        t.push(*character);

                        continue;
                    },
                    LexerType::Empty => {
                        match *character {
                            ' ' => {
                                // prevent a second empty
                                continue;
                            },
                            ':' => {
                                // key in object
                                buffer.push(LexerItem::from(LexerType::Colon, pos));
                                continue;
                            },
                            _ => ()
                        }
                    },
                    LexerType::MultilineComment(t) => {
                        if *character == '|' {
                            if c>0 && t.len() == 0 {
                                if let Some('#') = characters.get(c-1) {
                                    // ignore second comment indicatior
                                    continue;
                                }
                            }
                        } else if *character == '#' {
                            // emd of comment detection
                            if let Some('|') = t.chars().last() {
                                t.pop();
                                buffer.push(LexerItem::from(LexerType::Empty, pos));
                                continue;
                            }
                        }

                        t.push(*character);
                        continue;
                    },
                    LexerType::Comment(t) => {
                        t.push(*character);
                        continue;
                    },
                    LexerType::Variable(v) => {
                        let reached_end = match character {
                            ' ' => true,
                            ')' => true,
                            '\n' => true,
                            '#' => true,
                            _ => false
                        };

                        if reached_end {
                            // parse variable content
                            match v.as_str() {
                                "t" => {
                                    // boolean (true)
                                    let old = buffer.pop().unwrap();

                                    buffer.push(LexerItem::from(
                                        LexerType::Boolean(true),
                                        old.position
                                    ));
                                },
                                "nil" => {
                                    //boolean (false)
                                    let old = buffer.pop().unwrap();

                                    buffer.push(LexerItem::from(
                                        LexerType::Boolean(false),
                                        old.position
                                    ));
                                },
                                other => {
                                    let value = other.to_string();
                                    // might be number
                                    if let Ok(i) = value.parse::<isize>() {
                                        let old = buffer.pop().unwrap();

                                        // is int
                                        buffer.push(LexerItem::from(
                                            LexerType::Integer(i),
                                            old.position
                                        ));
                                    } else if let Ok(f) = value.parse::<f64>() {
                                        let old = buffer.pop().unwrap();
                                        // is float
                                        buffer.push(LexerItem::from(
                                            LexerType::Float(f),
                                            old.position
                                        ));
                                    }
                                }
                            }
                        } else {
                            // add char to variable name
                            v.push(*character);
                            continue;
                        }
                    },
                    _ => ()
                }
            }

            if *character == '#'{
                if let Some(c) = characters.get(c+1) {
                    if *c == '|' {
                        // begin multiline comment
                        buffer.push(LexerItem::from(
                            LexerType::MultilineComment(String::new()),
                            pos
                        ));
                        continue;
                    }
                }
            }

            match character {
                '(' => {
                    // opening bracket
                    buffer.push(LexerItem::from(
                        LexerType::OpeningBracket(level),
                        pos
                    ));
                    level+=1;
                },
                ')' => {
                    // closing bracket
                    if level<1 {
                        return Err(LexerError::UnmatchedClosingBracket(pos));
                    }
                    level-=1;
                    buffer.push(LexerItem::from(
                        LexerType::ClosingBracket(level),
                        pos
                    ));
                },
                '\'' => {
                    // symbol or list
                    buffer.push(LexerItem::from(
                        LexerType::SingleQuote,
                        pos
                    ));
                },
                ' ' => {
                    // space
                    buffer.push(LexerItem::from(
                        LexerType::Empty,
                        pos
                    ));
                },
                '&' => {
                    // note
                    buffer.push(LexerItem::from(
                        LexerType::Ampersand,
                        pos
                    ));
                },
                ';' => {
                    // single line comment
                    buffer.push(LexerItem::from(
                        LexerType::Comment(String::new()),
                        pos
                    ));
                },
                '"' => {
                    // text
                    buffer.push(LexerItem::from(
                        LexerType::Text(String::new()),
                        pos
                    ));
                },
                other => {
                    buffer.push(LexerItem::from(
                        LexerType::Variable(String::from(*other)),
                        pos
                    ));
                }
            }
        }
        if let Some(last) = &buffer.last().clone() {
            match last.item {
                LexerType::Text(_) => {
                    if let Some(last) = &mut buffer.last_mut() {
                        if let LexerType::Text(t) = &mut last.item {
                            t.push('\n');
                        }
                    }
                },
                LexerType::Empty => (),
                LexerType::MultilineComment(_) => (),
                _ => buffer.push(LexerItem::from(
                    LexerType::Empty,
                    Position::from_xy(characters.len(), l)
                ))
            }
        }
    }

    if level != 0 {
        return Err(LexerError::UnmatchedOpeningBracket);
    }

    if let Some(last) = buffer.last() {
        if let LexerType::Empty = last.item {

            buffer.pop();
        }
    }

    Ok(buffer)
}
