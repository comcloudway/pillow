use std::{
    collections::HashMap,
    sync::Arc,
    cell::RefCell, borrow::Borrow
};

use super::parser::{
    ParserType,
    ParserItem
};
use crate::types::Position;

#[derive(Clone, Debug)]
pub enum PillowType {
    /// a list of commands to be executed
    CommandList(Vec<PillowStack>),

    /// a command
    /// where the first item in the array is the function name
    /// and the other items are function arguments
    /// e.g. (+ 1 2)
    Command(Vec<PillowStack>),
    /// a function definition
    /// used to store functions in memory
    /// the first argument is a list of arguments
    /// the second argument is either a command or a commandlist
    /// supposed to be constructed by defun
    Function {
        /// the context inside of the function
        /// as a child of the context in which the function was declared
        inner: PillowContextStack,
        // the context in which the function has been called
        // used to resolve function arguments
        //outer: PillowContextStack,
        /// list of function arguments
        /// can be of types: Variable,OptionalMarker,Command
        args: Vec<PillowStack>,
        /// code to be interpreted
        /// will most likely be a commandlist
        /// but can be any of PillowType
        commands: PillowStack
    },
    /// a boolean
    /// can be true -> t
    /// or false -> nil
    Boolean(bool),
    /// a number value
    /// without a floating point
    /// always assumed to be either positive or negative
    /// e.g. 6
    Integer(isize),
    /// a floating point number
    /// e.g. 4.5
    Float(f64),
    /// a list of other parser types
    /// created from '(...items)
    List(Vec<PillowStack>),
    /// a string
    /// e.g. "...text"
    Text(String),
    /// a symbol
    /// similar to text, but cannot contain spaces
    /// e.g. 'car
    Symbol(String),
    /// a variable
    /// can be used for function name or variables
    /// keyword might also be referred to as variables
    Variable(String),
    /// the only currently supported keyword/note
    /// indicates that the following function argument is optional
    /// and does not have to exist
    /// NOTE: the function argument might look like this:
    /// &optional (key "default_value")
    /// providing a default value for the given key
    /// the interpreter should format this as Option<ParserType>
    /// only falling back to none if the argument is not provided
    /// otherwise relying on the default value
    OptionalMarker
}
impl PillowType {
    pub fn from(pt: ParserType) -> PillowType {
        match pt {
            ParserType::OptionalMarker => PillowType::OptionalMarker,
            ParserType::Variable(v) => PillowType::Variable(v),
            ParserType::Symbol(s) => PillowType::Symbol(s),
            ParserType::Text(t) => PillowType::Text(t),
            ParserType::Float(f) => PillowType::Float(f),
            ParserType::Integer(i) => PillowType::Integer(i),
            ParserType::CommandList(l) => PillowType::CommandList(
                l.iter().map(|d|Arc::new(RefCell::new(PillowItem::from(d.clone())))).collect()
            ),
            ParserType::List(l) => PillowType::List(
                l.iter().map(|d|Arc::new(RefCell::new(PillowItem::from(d.clone())))).collect()
            ),
            ParserType::Command(c) => PillowType::Command(
                c.iter().map(|d|Arc::new(RefCell::new(PillowItem::from(d.clone())))).collect()
            ),
            ParserType::Boolean(b) => PillowType::Boolean(b)
        }
    }
    pub fn to_string(&self) -> Option<String> {
        match self {
            PillowType::Text(t) => Some(t.to_string()),
            PillowType::Boolean(b) => Some(if *b {
                "t".to_string()
            } else {
                "nil".to_string()
            }),
            PillowType::Float(f) => Some(f.to_string()),
            PillowType::Integer(i) => Some(i.to_string()),
            _ => None
        }
    }
}

#[derive(Clone, Debug)]
pub struct PillowItem {
    pub position: Option<Position>,
    pub item: PillowType
}
impl PillowItem {
    pub fn from(p: ParserItem) -> Self {
        Self {
            position: Some(p.position),
            item: PillowType::from(p.item)
        }
    }
    pub fn from_type(p: PillowType) -> Self {
        Self {
            position: None,
            item: p
        }
    }
    pub fn with_pos(mut self,pos: Position) -> Self {
        self.position = Some(pos);
        self
    }
}
pub type PillowStack = Arc<RefCell<PillowItem>>;

#[derive(Clone, Debug)]
pub enum PillowError {
    /// a given input expression could not be executed
    FailedToParseExpression(PillowStack),
    /// a given command was not found
    CommandNotFound(PillowStack),
    /// a custom error message
    /// for lazy devs
    Custom(String, Option<Position>),

    /// function has not been found in context
    UnknownFunction(PillowStack),
    /// variable not found in context
    UnknownVariable(PillowStack),

    UnknownError,
    /// a keyword indicated that more data should be provided
    /// but the data was never recieved
    /// e.g. &optional without a key
    UnexpectedEndOfInput(PillowStack),

    /// invalid variable type
    InvalidType(PillowStack),
    /// a given function received too many
    /// the first argument is the command which has too many args
    /// the second argument is the amount of arguments it expected
    /// and the third argument is the amount of arguments it received
    TooManyArguments(PillowStack, usize, usize),
    /// a given function did not receive enough arguments
    /// the first argument is the command which does not have enough args
    /// the second argument is the amount of arguments it expected
    /// and the third argument is the amount of arguments it received
    NotEnoughArguments(PillowStack, usize, usize),
}

pub type HostCommand = Box<dyn FnMut(PillowContextStack, Vec<PillowStack>) -> Result<PillowStack,PillowError> + Sync + Send + 'static>;
static mut HOST_COMMANDS: Option<HashMap<String, HostCommand>> = None;

pub struct HostCommandBuilder {
    state: HashMap<String, HostCommand>
}
impl HostCommandBuilder {
    pub fn build(self) -> HashMap<String, HostCommand> {
        self.state
    }

    pub fn apply(self) {
        unsafe {
            HOST_COMMANDS = Some(self.build());
        }
    }
    pub fn template(t: HashMap<String, HostCommand>) -> Self {
        Self {
            state: t
        }
    }
    pub fn new() -> Self {
        Self {
            state: HashMap::new()
        }
    }
    pub fn with(mut self, name: String, func: HostCommand) -> Self {
        self.state.insert(name, func);
        self
    }
}

#[derive(Clone, Debug)]
pub struct PillowContext {
    state: HashMap<String, PillowStack>,
    parent: Option<PillowContextStack>
}
impl PillowContext {
    /// creates a new empty context
    /// as it has no parent it is the root/global context
    /// use fork to fork a global context into child contexts
    pub fn new() -> Self {
        Self {
            state: HashMap::new(),
            parent: None
        }
    }

    /// creates a child context
    /// you can fork root/global and child contexts
    pub fn fork(this: PillowContextStack) -> PillowContextStack {
        Arc::new(RefCell::new(PillowContext {
            state: HashMap::new(),
            parent: Some(this.clone())
        }))
    }

    /// stores an entry in the context
    /// by overwriting the old memory location
    /// similar to cloning
    pub fn store<S>(this: PillowContextStack, key: S, value: PillowStack) where S: Into<String> {
        this.as_ref().borrow_mut()
            .state.insert(key.into(), value);
    }

    /// stores an entry in the context
    /// by updating the data in the old location
    /// similar to pointers
    /// falls back to store if entry does not exist
    pub fn update<S>(
        this: PillowContextStack,
        key: S,
        value: PillowItem
    ) where S: Into<String> {
        let key = key.into();
        if let Some(entry) = &this.clone()
                                .as_ref().borrow_mut()
                                .state.get_mut(&key.to_string()) {
            *entry.borrow_mut()=value;
                                    return;
        }

            PillowContext::store(
                this,
                key,
                Arc::new(RefCell::new(value))
            );
    }

    /// finds the global context
    /// aka the top level context
    pub fn extract_global(this: PillowContextStack) -> PillowContextStack {
        if let Some(parent) = &this.clone().as_ref().borrow().parent {
            PillowContext::extract_global(parent.clone())
        } else {
            this
        }
    }

    /// attempts to find variable definition in context tree
    /// returns none if definitions has not been found
    /// returns the context if the variable has been defined in the given context
    pub fn find<S>(this: PillowContextStack, key: S) -> Option<PillowContextStack> where S: Into<String> {
        let key = key.into();
        if this.as_ref().borrow().state.borrow().contains_key(&key) {
            Some(this)
        } else if let Some(parent) = &this.as_ref().borrow().parent {
            PillowContext::find(parent.clone(), key)
        } else {
            None
        }
    }
}
pub type PillowContextStack = Arc<RefCell<PillowContext>>;

pub fn interpret(input: PillowStack, ctx: PillowContextStack) -> Result<PillowStack, PillowError> {
    match &input.as_ref().borrow().item {
        PillowType::CommandList(list) => {
            let mut ret = Arc::new(RefCell::new(PillowItem::from_type(PillowType::Boolean(false))));

            for cmd in list {
                match interpret(cmd.clone(), ctx.clone()) {
                    Ok(r) => ret = r,
                    Err(e) => return Err(e)
                }
            }

            Ok(ret)
        },
        PillowType::List(list) => {
            let mut buffer = Vec::new();

            for item in list {
                match interpret(item.clone(), ctx.clone()) {
                    Ok(r) => buffer.push(r),
                    Err(e) => return Err(e)
                }
            }

            Ok(Arc::new(RefCell::new(
                PillowItem::from_type(PillowType::List(list.to_vec()))
            )))
        },
        PillowType::Variable(name) => {
            if let Some(c) = PillowContext::find(ctx.clone(), name) {
                interpret(
                    c.as_ref().borrow().state.get(name).unwrap().clone(),
                    ctx.clone())
            } else {
                Err(PillowError::UnknownVariable(input.clone()))
            }
        },
        PillowType::Command(args) => {
            let outer = ctx.clone();
            if let Some(name) = args.first() {
                let name = match &name.as_ref().borrow().item {
                    PillowType::Variable(n) => n.to_string(),
                    _ => return Err(PillowError::InvalidType(name.clone()))
                };
                if let Some(c) = PillowContext::find(ctx.clone(), &name) {
                    match &c.as_ref().borrow().state.get(&name).unwrap().as_ref().borrow().item {
                        PillowType::Function {
                            inner:ctx,
                           // outer,
                            args:fargs,
                            commands:code
                        } => {
                            let mut max_args = 0;
                            let mut min_args = 0;
                            let mut index = 0;
                            while let Some(arg) = fargs.get(index) {
                                match &arg.as_ref().borrow().item {
                                    PillowType::Variable(_) => {
                                        min_args+=1;
                                        max_args+=1;
                                    },
                                    PillowType::OptionalMarker => {
                                        index+=1;
                                        max_args+=1;
                                    },
                                    _ => return Err(PillowError::InvalidType(args.get(index).unwrap().clone()))
                                }
                                index+=1;
                            }

                            if args.len() < min_args+1 {
                                return Err(PillowError::NotEnoughArguments(
                                    input.clone(),
                                    min_args, fargs.len()-1));
                            } else if args.len() > max_args+1 {
                                return Err(PillowError::TooManyArguments(
                                    input.clone(),
                                    max_args, fargs.len()-1));
                            }

                            let ctx = PillowContext::fork(ctx.clone());
                            let mut index = 0;
                            let mut findex = 1;
                            while let Some(arg) = fargs.get(index) {
                                match &arg.as_ref().borrow().item {
                                    PillowType::Variable(name) => {
                                        match interpret(args.get(findex).unwrap().clone(), outer.clone()) {
                                            Ok(dt) => {
                                                PillowContext::store(ctx.clone(), name, dt);
                                            },
                                            Err(e) => return Err(e)
                                        }
                                    },
                                    PillowType::OptionalMarker => {
                                        index+=1;
                                        if let Some(arg) = fargs.get(index) {
                                            match &arg.as_ref().borrow().item {
                                                PillowType::Variable(name) => {
                                                    if let Some(inp) = args.get(findex) {
                                                        match interpret(inp.clone(), outer.clone()) {
                                                            Ok(dt) => {
                                                                PillowContext::store(ctx.clone(), name, dt);
                                                            },
                                                            Err(e) => return Err(e)
                                                        }
                                                    }
                                                },
                                                PillowType::Command(list) => {
                                                    if args.len() < min_args+1 {
                                                        return Err(PillowError::NotEnoughArguments(
                                                            input.clone(),
                                                            2, list.len()));
                                                    } else if args.len() > max_args+1 {
                                                        return Err(PillowError::TooManyArguments(
                                                            input.clone(),
                                                            2, list.len()));
                                                    }

                                                    let name = match &list.first().unwrap().as_ref().borrow().item {
                                                        PillowType::Variable(name) => name.to_string(),
                                                        _ => return Err(PillowError::InvalidType(list.first().unwrap().clone()))
                                                    };

                                                    if let Some(inp) = args.get(findex) {
                                                        match interpret(inp.clone(), outer.clone()) {
                                                            Ok(dt) => {
                                                                PillowContext::store(ctx.clone(), name, dt);
                                                            },
                                                            Err(e) => return Err(e)
                                                        }
                                                    } else {
                                                        // default
                                                        match interpret(list.last().unwrap().clone(), outer.clone()) {
                                                            Ok(dt) => {
                                                                PillowContext::store(ctx.clone(), name, dt);
                                                            },
                                                            Err(e) => return Err(e)
                                                        }
                                                    }
                                                },
                                                _ => return Err(PillowError::InvalidType(args.get(index).unwrap().clone()))
                                            }
                                        } else {
                                            return Err(PillowError::UnexpectedEndOfInput(arg.clone()))
                                        }
                                    },
                                    _ => return Err(PillowError::InvalidType(args.get(index).unwrap().clone()))
                                }
                                index+=1;
                                findex+=1;
                            }

                            return interpret(code.clone(), ctx.clone());
                        },
                        _ => return Err(PillowError::InvalidType(c.as_ref().borrow().state.get(&name).unwrap().clone()))
                    }
                }

                unsafe {
                    // check if command is of type HostCommand
                    if let Some(list) = &mut HOST_COMMANDS {
                        if let Some(cmd) = &mut list.get_mut(&name) {
                            return (**cmd)(PillowContext::fork(ctx.clone()), args.to_vec())
                        }
                    }
                }

                Err(PillowError::UnknownFunction(input.clone()))
            } else {
                Err(PillowError::NotEnoughArguments(
                    input.clone(), 1, 0))
            }
        },
        _ => Ok(input.clone())
    }
}
