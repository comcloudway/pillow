use super::lexer::{
    LexerOutput,
    LexerType,
    LexerItem
};
use crate::types::Position;

#[derive(Clone, Debug)]
pub enum ParserType {
    /// a list of commands to be executed
    CommandList(Vec<ParserItem>),

    /// a command
    /// where the first item in the array is the function name
    /// and the other items are function arguments
    /// e.g. (+ 1 2)
    Command(Vec<ParserItem>),
    /// a boolean
    /// can be true -> t
    /// or false -> nil
    Boolean(bool),
    /// a number value
    /// without a floating point
    /// always assumed to be either positive or negative
    /// e.g. 6
    Integer(isize),
    /// a floating point number
    /// e.g. 4.5
    Float(f64),
    /// a list of other parser types
    /// created from '(...items)
    List(Vec<ParserItem>),
    /// a string
    /// e.g. "...text"
    Text(String),
    /// a symbol
    /// similar to text, but cannot contain spaces
    /// e.g. 'car
    Symbol(String),
    /// a variable
    /// can be used for function name or variables
    /// keyword might also be referred to as variables
    Variable(String),
    /// the only currently supported keyword/note
    /// indicates that the following function argument is optional
    /// and does not have to exist
    /// NOTE: the function argument might look like this:
    /// &optional (key "default_value")
    /// providing a default value for the given key
    /// the interpreter should format this as Option<ParserType>
    /// only falling back to none if the argument is not provided
    /// otherwise relying on the default value
    OptionalMarker
}
impl ParserType {
    pub fn from(li: LexerItem) -> Result<ParserType, ParserError> {
        match li.item {
            LexerType::Boolean(b) => Ok(ParserType::Boolean(b)),
            LexerType::Integer(i) => Ok(ParserType::Integer(i)),
            LexerType::Float(f) => Ok(ParserType::Float(f)),
            LexerType::Text(t) => Ok(ParserType::Text(t)),
            LexerType::Variable(v) => Ok(ParserType::Variable(v)),
            lt => Err(ParserError::FailedToConvertLexerType(lt, li.position))
        }
    }
}

#[derive(Clone, Debug)]
pub struct ParserItem {
    /// position of item source
    /// used for error reporting
    pub position: Position,
    /// the item type and it's contents
    pub item: ParserType
}
impl ParserItem {
    /// autogenerate a parser item
    /// from a type and it's original source code position
    pub fn from(item: ParserType, pos: Position) -> Self {
        Self { position: pos, item }
    }
}

#[derive(Clone, Debug)]
pub enum ParserError {
    /// the parser failed to generate a ParserItem
    /// thrown if parse has none as its buffers value
    /// after running through the algorithm
    FailedToCreateItem(LexerOutput),
    /// thrown if the given lexertype could not be automatically cast
    FailedToConvertLexerType(LexerType, Position),
    /// thrown in buffer has an incompatible type
    /// e.g. trying to add a number to a command list
    BufferHoldsIncompatibleType(ParserItem, ParserItem),
    /// thrown if first element of command is not of type variable
    InvalidFunctionName(ParserItem)
}

/// attempts to parse the lexed input
/// calling parse on a lexed version of a command should return the Command type
/// when called on a list of commands it should return CommandList
/// otherwise it should attempt to convert the lexed data into a ParserType
pub fn parse(input: LexerOutput, prefill_buffer: Option<ParserItem>) -> Result<ParserItem, ParserError> {
    let mut index = 0;

    let mut buffer:Option<ParserItem> = prefill_buffer;

    while let Some(item) = input.get(index) {
        match &item.item {
            LexerType::OpeningBracket(id) => {
                // find matching closing bracket
                // move index pointers inward and rerun
                // passing ParserType::Command as prefill argument

                let mut r = index;
                while let Some(item) = input.get(r) {

                    if let LexerType::ClosingBracket(c) = item.item {
                        if c == *id {
                            break;
                        }
                    }

                    r+=1;
                }

                let pos = item.position.clone();

                let mut template = ParserItem::from(ParserType::Command(Vec::new()), pos.clone());
                if index > 0 {
                    if let Some(item) = input.get(index - 1) {
                        if let LexerType::SingleQuote = item.item {
                            template = ParserItem::from(ParserType::List(Vec::new()), pos.clone());
                        }
                    }
                }

                match parse(
                    input.clone()[index+1..r].to_vec(),
                    Some(template)
                ) {
                    Ok(payload) => {
                        if let Some(ex) = &mut buffer {
                            match &mut ex.item {
                                ParserType::CommandList(l) => {
                                    l.push(payload);
                                },
                                ParserType::Command(a) => {
                                    if a.is_empty() {
                                        if let ParserType::Variable(_) = payload.item.clone() {
                                            a.push(payload);
                                        } else {
                                            return Err(
                                                ParserError::InvalidFunctionName(payload)
                                            )
                                        }
                                    } else {
                                        a.push(payload);
                                    }
                                },
                                ParserType::List(l) => {
                                    l.push(payload);
                                },
                                _ => return Err(
                                    ParserError::BufferHoldsIncompatibleType(
                                        ex.clone(),
                                        payload
                                    ))
                            }
                        } else {
                            buffer = Some(payload);
                        }

                        index = r;
                    },
                    Err(e) => return Err(e)
                }
            },
            LexerType::SingleQuote |
            LexerType::ClosingBracket(_) |
            LexerType::Empty |
            LexerType::Comment(_) |
            LexerType::MultilineComment(_) => (),
            _ => {
                match ParserType::from(item.clone()) {
                    Ok(cast) => {
                        let mut cast = cast;

                        if let ParserType::Variable(t) = &cast {
                            if index > 0 {
                                if let Some(item) = input.get(index-1) {
                                    match item.item {
                                        LexerType::SingleQuote => {
                                            // symbol
                                            cast = ParserType::Symbol(t.clone());
                                        },
                                        LexerType::Ampersand => {
                                            // note / maybe &optional
                                            if t.as_str() == "optional" {
                                                // &optional notice
                                                cast = ParserType::OptionalMarker
                                            }
                                        },
                                        _ => ()
                                    }
                                }
                            }
                        }

                        let pos = item.position.clone();

                        if let Some(ex) = &mut buffer {
                            match &mut ex.item {
                                ParserType::CommandList(l) => {
                                    if let ParserType::Command(_) = &cast {
                                        l.push(ParserItem::from(cast, pos));
                                    } else {
                                        return Err(
                                            ParserError::BufferHoldsIncompatibleType(
                                                ex.clone(),
                                                ParserItem::from(cast, pos)
                                            ));
                                    }
                                },
                                ParserType::Command(a) => {
                                    a.push(ParserItem::from(cast, pos));
                                },
                                ParserType::List(l) => {
                                    l.push(ParserItem::from(cast, pos));
                                },
                                _ => return Err(
                                    ParserError::BufferHoldsIncompatibleType(
                                        ex.clone(),
                                        ParserItem::from(cast, pos)
                                    ))
                            }
                        } else {
                            buffer = Some(ParserItem::from(cast, pos));
                        }
                    },
                    Err(e) => match item.clone().item {
                        LexerType::Ampersand | LexerType::SingleQuote => (),
                        _ => return Err(e)
                    }
                }
            }
        }
        index+=1;
    };

    if let Some(buf) = buffer {
        Ok(buf)
    } else {
        Err(ParserError::FailedToCreateItem(input))
    }
}
