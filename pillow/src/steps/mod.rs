pub mod lexer;
pub use lexer::lex;

pub mod parser;
pub use parser::parse;

pub mod interpreter;
pub use interpreter::interpret;
