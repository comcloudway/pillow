use crate::{PillowType, steps::interpreter::PillowStack, PillowItem};
use std::{
    sync::Arc,
    cell::RefCell
};

#[macro_export]
macro_rules! pillow {
    ( $target:expr, ) => ();
    ( $target:expr, $id:literal = $cmd:expr , $( $rest:tt )* ) => {
        {
            $target.insert(String::from($id),std::boxed::Box::new($cmd));
            pillow!($target, $($rest)*);
        }
    };
}

pub fn pillow_value(val: PillowType) -> PillowStack {
    Arc::new(
        RefCell::new(
            PillowItem::from_type(val)
        )
    )
}
