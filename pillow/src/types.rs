/// a position in the source file
/// indicated by a column and the row
#[derive(Debug, Clone)]
pub struct Position {
    /// column aka character
    /// represents the x axis
    col: usize,
    /// row aka line
    /// represents the y axis
    row: usize
}
impl Position {
    /// easily create position from x and y
    pub fn from_xy(x: usize, y: usize) -> Self{
        Self {
            col: x,
            row: y
        }
    }
    /// get x and y coordinates from position
    pub fn to_xy(&self) -> (usize, usize) {
        (self.col, self.row)
    }
}
