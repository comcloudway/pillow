pub mod steps;
use std::{
    sync::Arc,
    collections::HashMap,
    cell::RefCell
};

pub use crate::steps::{
    lex,
    parse,
    interpret
};
pub use crate::steps::lexer::{
    LexerOutput,
    LexerError
};
pub use crate::steps::parser::{
    ParserItem,
    ParserError,
    ParserType
};
pub use crate::steps::interpreter::{
    PillowStack,
    PillowContextStack,

    PillowContext,
    PillowItem,
    PillowType,
    PillowError,
    HostCommandBuilder,
    HostCommand,
};

mod types;
pub use types::Position;

pub mod macros;

pub struct Pillow;
pub struct PillowSource(String);
pub struct PillowLexed(LexerOutput);
pub struct PillowParsed {
    hold: ParserItem,
    state: HashMap<String, HostCommand>,
    pub ctx: PillowContextStack
}
#[derive(Debug)]
pub struct PillowOutput(pub PillowType);

impl Pillow {
    pub fn new() -> Self {
        Self
    }
    pub fn with_source(self, src: String) -> PillowSource {
        PillowSource::from(src)
    }
}
impl PillowSource {
    pub fn lex(self) -> Result<PillowLexed, LexerError> {
        match lex(self.0) {
            Ok(dt) => Ok(PillowLexed::from(dt)),
            Err(e) => Err(e)
        }
    }
    pub fn from(s: String) -> Self {
        PillowSource(s)
    }
}
impl PillowLexed {
    pub fn parse(self) -> Result<PillowParsed, ParserError> {
        match parse(self.0, Some(ParserItem::from(
            ParserType::CommandList(Vec::new()),
            Position::from_xy(0,0)
        ))) {
            Ok(dt) => Ok(PillowParsed::from(dt)),
            Err(e) => Err(e)
        }
    }
    pub fn from(l: LexerOutput) -> Self {
        PillowLexed(l)
    }
}
impl PillowParsed {
    pub fn from(p: ParserItem) -> Self {
        Self {
            hold: p,
            state: HashMap::new(),
            ctx: Arc::new(RefCell::new(PillowContext::new()))
        }
    }
    pub fn import(mut self, state: HashMap<String, HostCommand>) -> Self {
        self.state=state;
        self
    }
    pub fn with(mut self, key: String, value: HostCommand) -> Self {
        self.state.insert(key, value);
        self
    }
    pub fn exec(self) -> Result<PillowOutput, PillowError> {
        HostCommandBuilder::template(self.state)
            .apply();
        match interpret(
            Arc::new(RefCell::new(PillowItem::from(self.hold))),
            self.ctx
        ) {
            Ok(dt) => Ok(PillowOutput(dt.borrow().item.clone())),
            Err(e) => Err(e)
        }
    }
}
