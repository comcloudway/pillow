use crate::CommonLispTemplate;

use pillow::{
    PillowType,
    PillowItem,
    PillowError,
    interpret,
    pillow, macros::pillow_value
};

impl CommonLispTemplate {
    pub fn with_dummy(mut self) -> Self {
        pillow!(self.0,
                "dummy" = |ctx, args| {
                    Ok(pillow_value(PillowType::Boolean(false)))
                },
        );

        self
    }
}
