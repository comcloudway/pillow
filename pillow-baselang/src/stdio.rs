use crate::CommonLispTemplate;

use pillow::{
    PillowType,
    PillowItem,
    PillowStack,
    PillowError,
    interpret,
    pillow, macros::pillow_value,
};

impl CommonLispTemplate {
    pub fn with_stdio(mut self) -> Self {
        pillow!{self.0,
                "print" = |ctx, args| {
                    if args.len() > 2 {
                        return Err(PillowError::TooManyArguments(
                            pillow_value(PillowType::Command(args.to_vec())),
                            1,
                            args.len()-1)
                        );
                    }
                    if args.len() < 2 {
                        return Err(PillowError::TooManyArguments(
                            pillow_value(PillowType::Command(args.to_vec())),
                            1,
                            args.len()-1)
                        );
                    }

                    match interpret(args.last().unwrap().clone(), ctx) {
                        Err(e) => return Err(e),
                        Ok(dt) => match dt.as_ref().borrow().item.to_string() {
                            Some(s) => println!("{}", s),
                            None => return Err(PillowError::InvalidType(args.last().unwrap().clone()))
                        }
                    }

                    Ok(pillow_value(PillowType::Boolean(false)))
                },
                "read-line" = | _ctx, args | {
                    if args.len() > 1 {
                        return Err(PillowError::TooManyArguments(
                            pillow_value(PillowType::Command(args.to_vec())),
                            1,
                            args.len()-1)
                        );
                    }

                    let mut buf = String::new();
                    match std::io::stdin().read_line(&mut buf) {
                        Ok(_) => return Ok(pillow_value(PillowType::Text(buf))),
                        Err(e) => return Err(PillowError::Custom(e.to_string(), None))
                    }
                },
        };

        self
    }
}
