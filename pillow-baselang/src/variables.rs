use std::borrow::Borrow;

use crate::CommonLispTemplate;

use pillow::{
    PillowType,
    PillowItem,
    PillowError,
    interpret,
    pillow, macros::pillow_value, PillowContextStack, PillowStack, PillowContext
};

impl CommonLispTemplate {
    pub fn with_variables(mut self) -> Self {
        pillow!(self.0,
                "defvar" = |ctx, args| {
                    if args.len() < 3 {
                        return Err(PillowError::NotEnoughArguments(
                            pillow_value(PillowType::Command(args.to_vec())),
                            3,
                            args.len()-1)
                        );
                    }
                    if args.len() > 3 {
                        return Err(PillowError::TooManyArguments(
                            pillow_value(PillowType::Command(args.to_vec())),
                            3,
                            args.len()-1)
                        );
                    }
                    let key = args.get(1).unwrap().clone();
                    let value = args.get(2).unwrap().clone();

                    let ctx = PillowContext::extract_global(ctx);
                    let key = match &key.as_ref().borrow().item {
                        PillowType::Variable(name) => name.to_string(),
                        _ => return Err(PillowError::InvalidType(key.clone()))
                    };
                    let value = match interpret(value, ctx.clone()) {
                        Ok(dt) => dt,
                        Err(e) => return Err(e)
                    };

                    PillowContext::update(
                        ctx,
                        key,
                        value.as_ref().borrow().clone()
                    );

                    Ok(value)
                },
                "setq" = |ctx, args| {
                    if args.len() < 3 {
                        return Err(PillowError::NotEnoughArguments(
                            pillow_value(PillowType::Command(args.to_vec())),
                            3,
                            args.len()-1)
                        );
                    }
                    if args.len() > 3 {
                        return Err(PillowError::TooManyArguments(
                            pillow_value(PillowType::Command(args.to_vec())),
                            3,
                            args.len()-1)
                        );
                    }
                    let key = args.get(1).unwrap().clone();
                    let value = args.get(2).unwrap().clone();

                    let value = match interpret(value, ctx.clone()) {
                        Ok(dt) => dt,
                        Err(e) => return Err(e)
                    };
                    println!("{:?}", key);
                    let key = match interpret(key, ctx.clone()) {
                        Ok(dt) => dt,
                        Err(e) => return Err(e)
                    };

                    *key.as_ref().borrow_mut()=value.clone().as_ref().borrow().clone();

                    Ok(value)
                },
        );

        self
    }
}
