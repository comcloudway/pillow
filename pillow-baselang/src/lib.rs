use std::collections::HashMap;

use pillow::HostCommand;

#[cfg(target_os = "linux")]
pub mod stdio;
pub mod arithmetic;
pub mod functions;
pub mod variables;

pub struct CommonLispTemplate(HashMap<String, HostCommand>);
impl CommonLispTemplate {
    pub fn new() -> Self {
        Self(HashMap::new())
    }

    pub fn all(self) -> Self {
        self
            .with_arithmetic()
            .with_functions()
            .with_variables()
    }

    pub fn export(self) -> HashMap<String, HostCommand> {
        self.0
    }
}
pub fn common_lisp_template() -> HashMap<String, HostCommand> {
    CommonLispTemplate::new()
        .all()
        .export()
}
