use std::borrow::Borrow;

use crate::CommonLispTemplate;

use pillow::{
    PillowType,
    PillowItem,
    PillowError,
    interpret,
    pillow, macros::pillow_value, PillowContext
};

impl CommonLispTemplate {
    pub fn with_functions(mut self) -> Self {
        pillow!(self.0,
                "defun" = |ctx, args| {
                    if args.len() < 3 {
                        return Err(PillowError::NotEnoughArguments(
                            pillow_value(PillowType::Command(args.to_vec())),
                            3,
                            args.len()-1)
                        );
                    }

                    let mut args = args.to_vec();
                    args.remove(0); // remove defun
                    let name = args.remove(0); // the name of the function
                    let fargs = args.remove(0); // the arguments
                    let commands = args; // all other arguments of this function are commands

                    // parse input
                    let name = match &name.clone().as_ref().borrow().item {
                        PillowType::Variable(n) => n.to_string(),
                        _ => return Err(PillowError::InvalidType(name))
                    };
                    let fargs = match &fargs.clone().as_ref().borrow().item {
                        PillowType::Command(args) => args.to_vec(),
                        _ => return Err(PillowError::InvalidType(fargs))
                    };

                    PillowContext::store(
                        PillowContext::extract_global(ctx.clone()),
                        name,
                        pillow_value(PillowType::Function {
                            inner: ctx.clone(),
                            args: fargs.to_vec(),
                            commands: pillow_value(PillowType::CommandList(commands))
                        })
                    );

                    Ok(pillow_value(PillowType::Boolean(false)))
                },
                "return" = |_ctx, args| {
                    if args.len() < 1 {
                        return Err(PillowError::NotEnoughArguments(args.first().unwrap().clone(), 3-1, args.len()-1))
                    } else if args.len() > 2 {
                        return Err(PillowError::TooManyArguments(args.first().unwrap().clone(), 3-1, args.len()-1))
                    }
                    if let Some(ret) = args.last() {
                        return Ok(ret.clone());
                    }
                    Ok(pillow_value(PillowType::Boolean(false)))
                },
        );

        self
    }
}
