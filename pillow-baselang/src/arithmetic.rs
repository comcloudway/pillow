use crate::CommonLispTemplate;

use pillow::{
    PillowType,
    PillowError,
    interpret,
    pillow, macros::pillow_value, PillowStack
};

impl CommonLispTemplate {
    pub fn with_arithmetic(mut self) -> Self {
        pillow!(
            self.0,
            "+" = |ctx, args| {
                if args.len() < 3 {
                    return Err(PillowError::NotEnoughArguments(
                        pillow_value(PillowType::Command(args.to_vec())),
                        3,
                        args.len()-1)
                    );
                }

                let items = {
                    let items: Vec<Result<PillowStack, PillowError>> = args
                        .iter()
                        .skip(1)
                        .map(|d| interpret(d.clone(), ctx.clone()))
                        .collect();
                    let mut b = Vec::new();
                    for item in items {
                        match item {
                            Ok(dt) => b.push(dt),
                            Err(e) => return Err(e)
                        }
                    }
                    b
                };

                let mut has_ints = false;
                let mut has_floats = false;
                let mut has_string = false;
                for item in &items {
                    match item.clone().as_ref().borrow().item {
                        PillowType::Integer(_) => has_ints = true,
                        PillowType::Float(_) => has_floats = true,
                        PillowType::Text(_) => has_string = true,
                        _ => return Err(PillowError::InvalidType(item.clone()))
                    }
                }

                if has_string {
                    let mut buffer = String::new();

                    for item in items {
                        match item.clone().as_ref().borrow().item.to_string() {
                            Some(s) => buffer.push_str(&s),
                            None => return Err(PillowError::InvalidType(item.clone()))
                        }
                    }

                    Ok(pillow_value(PillowType::Text(buffer)))
                } else if has_floats {
                    let mut counter:f64 = 0.0;

                    for item in items {
                        match item.clone().as_ref().borrow().item {
                            PillowType::Integer(i) => counter += i as f64,
                            PillowType::Float(f) => counter+=f,
                            _ => return Err(PillowError::InvalidType(item.clone()))
                        }
                    }

                    Ok(pillow_value(PillowType::Float(counter)))

                } else if has_ints {
                    let mut counter:isize = 0;

                    for item in items {
                        match item.clone().as_ref().borrow().item {
                            PillowType::Integer(i) => counter+=i,
                            _ => return Err(PillowError::InvalidType(item.clone()))
                        }
                    }

                    Ok(pillow_value(PillowType::Integer(counter)))
                } else {
                    Err(PillowError::UnknownError)
                }
            },
            "-" = |ctx, args| {
                if args.len() < 3 {
                    return Err(PillowError::NotEnoughArguments(
                        pillow_value(PillowType::Command(args.to_vec())),
                        3,
                        args.len()-1)
                    );
                }

                let items = {
                    let items: Vec<Result<PillowStack, PillowError>> = args
                        .iter()
                        .skip(1)
                        .map(|d| interpret(d.clone(), ctx.clone()))
                        .collect();
                    let mut b = Vec::new();
                    for item in items {
                        match item {
                            Ok(dt) => b.push(dt),
                            Err(e) => return Err(e)
                        }
                    }
                    b
                };

                let mut has_ints = false;
                let mut has_floats = false;
                for item in &items {
                    match item.clone().as_ref().borrow().item {
                        PillowType::Integer(_) => has_ints = true,
                        PillowType::Float(_) => has_floats = true,
                        _ => return Err(PillowError::InvalidType(item.clone()))
                    }
                }

                if has_floats {
                    let mut counter:f64 = 0.0;

                    for (index,item) in items.iter().enumerate() {
                        match item.clone().as_ref().borrow().item {
                            PillowType::Integer(i) => counter += (i * if index==0 {1} else {-1}) as f64,
                            PillowType::Float(f) => counter+=f * if index==0 {1.} else {-1.},
                            _ => return Err(PillowError::InvalidType(item.clone()))
                        }
                    }

                    Ok(pillow_value(PillowType::Float(counter)))

                } else if has_ints {
                    let mut counter:isize = 0;

                    for (index,item) in items.iter().enumerate() {
                        match item.clone().as_ref().borrow().item {
                            PillowType::Integer(i) => counter+=i * if index==0 {1} else {-1},
                            _ => return Err(PillowError::InvalidType(item.clone()))
                        }
                    }

                    Ok(pillow_value(PillowType::Integer(counter)))
                } else {
                    Err(PillowError::UnknownError)
                }
            },
            "*" = |ctx, args| {
                if args.len() < 3 {
                    return Err(PillowError::NotEnoughArguments(
                        pillow_value(PillowType::Command(args.to_vec())),
                        3,
                        args.len()-1)
                    );
                }

                let items = {
                    let items: Vec<Result<PillowStack, PillowError>> = args
                        .iter()
                        .skip(1)
                        .map(|d| interpret(d.clone(), ctx.clone()))
                        .collect();
                    let mut b = Vec::new();
                    for item in items {
                        match item {
                            Ok(dt) => b.push(dt),
                            Err(e) => return Err(e)
                        }
                    }
                    b
                };

                let mut has_ints = false;
                let mut has_floats = false;
                for item in &items {
                    match item.clone().as_ref().borrow().item {
                        PillowType::Integer(_) => has_ints = true,
                        PillowType::Float(_) => has_floats = true,
                        _ => return Err(PillowError::InvalidType(item.clone()))
                    }
                }

                if has_floats {
                    let mut counter:f64 = 1.0;

                    for item in items {
                        match item.clone().as_ref().borrow().item {
                            PillowType::Integer(i) => counter *= i as f64,
                            PillowType::Float(f) => counter*=f,
                            _ => return Err(PillowError::InvalidType(item.clone()))
                        }
                    }

                    Ok(pillow_value(PillowType::Float(counter)))

                } else if has_ints {
                    let mut counter:isize = 1;

                    for item in items {
                        match item.clone().as_ref().borrow().item {
                            PillowType::Integer(i) => counter*=i,
                            _ => return Err(PillowError::InvalidType(item.clone()))
                        }
                    }

                    Ok(pillow_value(PillowType::Integer(counter)))
                } else {
                    Err(PillowError::UnknownError)
                }
            },
            "/" = |ctx, args| {
                if args.len() < 3 {
                    return Err(PillowError::NotEnoughArguments(
                        pillow_value(PillowType::Command(args.to_vec())),
                        3,
                        args.len()-1)
                    );
                }

                let items = {
                    let items: Vec<Result<PillowStack, PillowError>> = args
                        .iter()
                        .skip(1)
                        .map(|d| interpret(d.clone(), ctx.clone()))
                        .collect();
                    let mut b = Vec::new();
                    for item in items {
                        match item {
                            Ok(dt) => b.push(dt),
                            Err(e) => return Err(e)
                        }
                    }
                    b
                };

                for item in &items {
                    match item.clone().as_ref().borrow().item {
                        PillowType::Integer(_) | PillowType::Float(_) => (),
                        _ => return Err(PillowError::InvalidType(item.clone()))
                    }
                }

                let mut counter:f64 = 1.0;

                for (index,item) in items.iter().enumerate() {
                    if index == 0 {
                        match item.clone().as_ref().borrow().item {
                            PillowType::Integer(i) => counter = i as f64,
                            PillowType::Float(f) => counter=f,
                            _ => return Err(PillowError::InvalidType(item.clone()))
                        }
                    } else {
                        match item.clone().as_ref().borrow().item {
                            PillowType::Integer(i) => counter /= i as f64,
                            PillowType::Float(f) => counter/=f,
                            _ => return Err(PillowError::InvalidType(item.clone()))
                        }
                    }
                }

                Ok(pillow_value(PillowType::Float(counter)))

            },
        );

        self
    }
}
